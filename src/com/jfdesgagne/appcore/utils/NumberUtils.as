package com.jfdesgagne.appcore.utils {

	public class NumberUtils {

		public static function random( low:int, high:int ):int {
			return( Math.round(Math.random() * (high - low)) + low );	
		}

	}
}