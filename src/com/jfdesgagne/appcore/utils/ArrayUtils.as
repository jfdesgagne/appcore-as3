package com.jfdesgagne.appcore.utils {

	public class ArrayUtils {

		public static function clone( arr:Array ):Array {
			var newarr:Array = [];
			for( var i:int = 0;i < arr.length;i++ ) {
				newarr.push(arr[ i ]);
			}
			return( newarr );
		}

		public static function shuffle( arr:Array ):Array {			
			var last:int = arr.length - 1;
			var i:int = last + 1; 
			while( i-- ) { 
				var p:int = NumberUtils.random(0, last); 
				var t:Object = arr[ i ]; 
				arr[ i ] = arr[ p ]; 
				arr[ p ] = t; 
			} 
			return( arr );		
		}

		public static function removeItem( arr:Array, item:* ):Array {
			if(arr == null) return null;
			if(!arr is Array) return null;
			var index:int = arr.indexOf(item);
			if(index > -1) arr.splice(index, 1);
			return( arr );	
		}

		public static function itemExist(_value:*, _arr:Array):Boolean {
			for(var i:int = 0;i < _arr.length;i++) {
				if(_arr[i] == _value) return true;
			}
			return false;
		}
	}
}