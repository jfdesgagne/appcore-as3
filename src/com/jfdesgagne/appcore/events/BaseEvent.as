package com.jfdesgagne.appcore.events {
	import flash.events.Event;

	/**
	 * @author jfdesgagne
	 */
	public class BaseEvent extends Event {
		public function BaseEvent(_type:String) {
			super(_type, false, true);
		}
	}
}
