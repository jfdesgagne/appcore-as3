package com.jfdesgagne.appcore.events {

	/**
	 * @author jfdesgagne
	 */
	public class ViewEvent extends BaseEvent {
		public static const CHANGE_VIEW:String = "ViewEvent_CHANGE_VIEW";		public static const CLOSED:String = "ViewEvent_CLOSED";
		
		protected var __initParams: Object;
		protected var __view:*; //can be a Class or a instance of AbstractClassView

		public function ViewEvent(_type:String, _view:*=null, _initParams:Object=null) {
			//TODO: Validatio of view type
			//if(_view is Class || _view is AbstractBaseView)
			
			__view = _view;
			__initParams = _initParams;
			super(_type);
			
		}
		
		public function get initParams() : Object {
			return __initParams;
		}
		
		public function get view():* {
			return __view;
		}
	}
}
