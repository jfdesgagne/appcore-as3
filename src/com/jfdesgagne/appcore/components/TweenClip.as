package com.jfdesgagne.appcore.components {
	import com.jfdesgagne.appcore.assets.AppCoreAsset;

	import flash.display.FrameLabel;
	import flash.events.Event;
	import flash.utils.Dictionary;

	/**
	 * @author jdesgagne
	 */
	public class TweenClip extends AppCoreAsset {
		protected var __frameDictionary:Dictionary;
		protected var __gotoFrame:int;		protected var __reverse:Boolean;

		public function TweenClip() {
			super();
		}

		
		override public function assetReady():void {
			super.assetReady();
			stop();
			setupFrames();
		}

		private function setupFrames():void {
			__frameDictionary = new Dictionary(true);
			
			for (var i:uint = 0; i < currentLabels.length; i++) {
			    var label:FrameLabel = currentLabels[i];
			    __frameDictionary[label.name] = label.frame;
			}
		}

		protected function onRender(e:Event=null):void {
			if(__gotoFrame > -1) {
				if(__gotoFrame == currentFrame) {
					__gotoFrame = -1;
					removeEventListener(Event.ENTER_FRAME, onRender);
				} else {
					if(__reverse) {
						gotoAndStop(currentFrame - 1);
					} else {
						gotoAndStop(currentFrame + 1);
					}
				}
			} else {
				removeEventListener(Event.ENTER_FRAME, onRender);
			}
		}
		
		public function goto(_frame:String, _tween:Boolean=true):void {
			__gotoFrame = __frameDictionary[_frame];
			if(__gotoFrame == currentFrame) {
				__gotoFrame = -1;
			} else {
				if(_tween) {
					if(__gotoFrame < currentFrame) {
						__reverse = true;
					} else {
						__reverse = false;
					}
					
					addEventListener(Event.ENTER_FRAME, onRender);
				} else {
					gotoAndStop(__gotoFrame);
					__gotoFrame = -1;
				}
			}
		}

	}
}
