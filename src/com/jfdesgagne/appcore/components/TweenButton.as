package com.jfdesgagne.appcore.components {
	import com.jfdesgagne.appcore.core.AppCore;
	import flash.events.MouseEvent;

	/**
	 * @author jdesgagne
	 */
	public class TweenButton extends TweenClip {
		public static const STATE_OUT:String = "out";		public static const STATE_OVER:String = "over";
		public static const STATE_DOWN:String = "down";
		public static const STATE_DISABLED:String = "disabled";		public static const STATE_SELECTED:String = "selected";
		
		public function TweenButton() {
			super();
		}

		override public function assetReady():void {
			super.assetReady();
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver, false, 0, true );
			addEventListener(MouseEvent.ROLL_OUT, onMouseOut, false, 0, true );
			addEventListener(MouseEvent.CLICK, onMouseClick, false, 0, true );
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false, 0, true );
		}

		protected function onMouseUp(event:MouseEvent):void {
			goto(STATE_OUT, false);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
			AppCore.stageRef.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		protected function onMouseDown(event:MouseEvent):void {
			goto(STATE_DOWN, false);
			AppCore.stageRef.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
		}

		protected function onMouseClick(event:MouseEvent):void {
			goto(STATE_DOWN);
		}

		protected function onMouseOut(event:MouseEvent):void {
			goto(STATE_OUT);
		}

		protected function onMouseOver(event:MouseEvent):void {
			goto(STATE_OVER);
		}

		
		override public function destroy():void {
			AppCore.stageRef.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
			removeEventListener(MouseEvent.CLICK, onMouseClick);
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			
			super.destroy();
		}
	}
}
