package com.jfdesgagne.appcore.components {
	import com.jfdesgagne.appcore.assets.AppCoreAsset;

	import flash.display.DisplayObject;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	/**
	 * @author jfdesgagne
	 */
	public class TextLabel extends AppCoreAsset {
		protected var __textField:TextField;
		protected var __autoSize:Boolean = true;
		
		public function TextLabel() {
			super();
		}

		override public function assetReady():void {
			var child:DisplayObject;
			for(var i:int=0; i<numChildren; i++) {
				child = getChildAt(i);
				if(child is TextField) {
					if(__textField) {
						throw new Error("[TextLabel] An TextLabel can't have more than one TextField");
					} else {
						__textField = child as TextField;
					}
				}
			}
			
			if(__textField == null) throw new Error("[TextLabel] An TextLabel must have one TextField");
			
			super.assetReady();
		}

		public function set text(_text:String):void {
			__textField.text = _text;
			update();
		}
		
		public function update():void {
			if(__autoSize) {
				var textFormat:TextFormat = __textField.getTextFormat();
				__textField.autoSize = textFormat.align;
			}
		}
		
		public function get autoSize():Boolean {
			return __autoSize;
		}
	}
}
