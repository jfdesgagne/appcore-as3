package com.jfdesgagne.appcore.statics {

	/**
	 * @author jfdesgagne
	 */
	public class ViewTypesStatics {
		public static const TYPE_VIEW:int = 0;		public static const TYPE_MODAL:int = 1;		public static const TYPE_PROMPT:int = 2;		public static const TYPE_ERROR:int = 3;
	}
}
