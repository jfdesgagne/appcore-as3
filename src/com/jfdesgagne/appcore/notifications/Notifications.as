package com.jfdesgagne.appcore.notifications {
	import flash.events.Event;
	import flash.utils.Dictionary;

	/**
	 * @author jfdesgagne
	 */
	public class Notifications {
		public static var __notified:Dictionary;
		
		public function Notifications():void {}
		
		public static function addNotification(_type:String, _callback:Function):void {
			getNotification(_type).addDispatcher(_callback);
		}
		
		public static function removeNotification(_type:String, _callback:Function):void {
			getNotification(_type).removeDispatcher(_callback);
		}
		
		public static function removeAllNotification(_type:String):void {
			getNotification(_type).removeAllDispatcher();
		}
		
		public static function notify(_e:Event):void {
			getNotification(_e.type).sendNotification(_e);
		}
		
		public static function getNotification(_type:String):Notified {
			if(__notified == null) __notified = new Dictionary(true);
			if(__notified[_type] == null) {
				__notified[_type] = new Notified(_type);
			}
			
			return __notified[_type];
		}
		
		public static function hasNotifications(_type:String):Boolean {
			if(getNotification(_type).numNotifications > 0) {
				return true;
			} else {
				return false;
			}
		}
	}
}
