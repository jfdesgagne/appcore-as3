package com.jfdesgagne.appcore.notifications {
	import flash.events.Event;
	import flash.utils.Dictionary;

	/**
	 * @author jfdesgagne
	 */
	public class Notified {
		protected var __event:String;
		protected var __dispatchers:Dictionary;
		
		public function Notified(_event:String):void {
			__event = _event;
			__dispatchers = new Dictionary(true);
		}
		
		public function get event() : String {
			return __event;
		}
		
		public function set event(_event : String) : void {
			__event = _event;
		}
		
		public function addDispatcher(_function:Function):void {
			__dispatchers[_function] = _function;
		}
		
		public function removeDispatcher(_function:Function):void {
			delete __dispatchers[_function];
		}

		public function removeAllDispatcher() : void {
			for each(var dispatchFunction:Function in __dispatchers) {
				delete __dispatchers[dispatchFunction];
			}
			
			__dispatchers = new Dictionary(true);
		}
		
		public function get numNotifications():int {
			var count:int = 0;
			for each(var dispatchFunction:Function in __dispatchers) {
				count++;
			}
			return count;
		}

		public function sendNotification(_e:Event) : void {
			for each(var dispatchFunction:Function in __dispatchers) {
				dispatchFunction(_e); 
			}
		}
	}
}
