package com.jfdesgagne.appcore.manager.view {
	import com.jfdesgagne.appcore.view.AbstractBaseView;

	import flash.display.Sprite;
	import flash.utils.getQualifiedClassName;

	/**
	 * @author jfdesgagne
	 */
	public class ViewContainer extends Sprite {
		protected var __views:Array;
		protected var __type:int;
		
		//pending
		protected var __pendingView:AbstractBaseView;
		protected var __pendingInitParams:Object;
		
		
		public function ViewContainer(_type:int) {
			__type = _type;
			__views = [];
		}

		public function addView(_view:AbstractBaseView, _initParams:Object):void {
			if(!getView(_view)) {
				__views.push(_view);
				addChild(_view.display);
				_view.initialize(_initParams);
			}
		}
		
		public function removeView(_view:AbstractBaseView):void {
			var index:int = __views.indexOf(_view);
			if(index > -1) {
				__views.splice(index, 1);
				removeChild(_view.display);
				_view.destroy();
				_view = null;
			}
			
			verifyPendingView();
		}
		
		protected function verifyPendingView():void {
			if(__pendingView) {
				addView(__pendingView, __pendingInitParams);
			}
			
			__pendingView = null;
			__pendingInitParams = null;
		}
		
		public function addPendingView(_view:AbstractBaseView, _initParams:Object):void {
			__pendingView = _view;
			__pendingInitParams = _initParams;
		}
		
		public function getView(_view:AbstractBaseView):AbstractBaseView {
			for each(var view:AbstractBaseView in __views) {
				if(getQualifiedClassName(view) == getQualifiedClassName(_view)) {
					return view;
				}
			}
			
			return null;
		}
		
		public function numViews():int {
			return __views.length;
		}
	}
}
