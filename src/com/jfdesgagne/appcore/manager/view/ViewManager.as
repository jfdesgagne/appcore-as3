package com.jfdesgagne.appcore.manager.view {
	import com.jfdesgagne.appcore.events.ViewEvent;
	import com.jfdesgagne.appcore.notifications.Notifications;
	import com.jfdesgagne.appcore.view.AbstractBaseView;

	import flash.display.Sprite;
	import flash.utils.Dictionary;

	/**
	 * @author jfdesgagne
	 */
	public class ViewManager extends Sprite {
		private static var __instance:ViewManager;
		
		protected var __currentView:AbstractBaseView;
		protected var __nextView:Class;
		protected var __nextInitParams:Object;
		
		protected var __viewContainers:Dictionary;

		public function ViewManager():void {
			if(__instance == null) {
				init();
			} else {
				throw new Error("[ViewManager] - Can't Instantiate More than Once");
			}
		}
		
		public static function getInstance():ViewManager {
			if (__instance == null) {
				__instance = new ViewManager();
			}
			
			return __instance;
		}

		protected function init():void {
			__viewContainers = new Dictionary(true);
			
			Notifications.addNotification(ViewEvent.CLOSED, onViewClosed);			Notifications.addNotification(ViewEvent.CHANGE_VIEW, onViewChanged);
		}
		
		protected function onViewChanged(_e:ViewEvent):void {
			changeView(_e.view, _e.initParams);
		}
		
		protected function getViewContainer(_viewType:int):ViewContainer {
			if(__viewContainers[_viewType] == null) {
				__viewContainers[_viewType] = new ViewContainer(_viewType);
				addChildAt(__viewContainers[_viewType], _viewType);
			}
			
			return __viewContainers[_viewType];
		}
		
		protected function removeView(_view:AbstractBaseView):void {
			getViewContainer(_view.viewType).removeView(_view);
		}
		
		protected function addView(_view:AbstractBaseView, _initParams:Object):void {
			getViewContainer(_view.viewType).addView(_view, _initParams);
		}

		protected function onViewClosed(_e:ViewEvent):void {
			removeView(_e.view);
		}

		public function changeView(_view:Class, _initParams:Object=null):void {
			var instance:AbstractBaseView = new _view();
			if(instance.isUnique) {
				var currentView:AbstractBaseView = getView(instance);
				if(currentView) {
					getViewContainer(instance.viewType).addPendingView(instance, _initParams);
					currentView.close();
				} else {
					addView(instance, _initParams);
				}
			} else {
				addView(instance, _initParams);
			}
		}

		private function getView(_view:AbstractBaseView):AbstractBaseView {
			var viewContainer:ViewContainer = getViewContainer(_view.viewType);
			return viewContainer.getView(_view);
		}

		protected function isViewContainerEmpty(_viewType:int):Boolean {
			if(getViewContainer(_viewType).numChildren > 0) return true;
			return false;
		}
	}
}
