package com.jfdesgagne.appcore.manager.queuegroup.events {
	import flash.events.Event;

	/**
	 * @author jdesgagne
	 */
	public class QueueGroupEvent extends Event {
		public static const FUNCTION_COMPLETE:String = "QueueGroupEvent_FUNCTION_COMPLETE";		public static const GROUP_COMPLETE:String = "QueueGroupEvent_GROUP_COMPLETE";		public static const ALL_COMPLETE:String = "QueueGroupEvent_ALL_COMPLETE";
		protected var __group:int;
		
		public function QueueGroupEvent(type:String, _group:int=0, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			__group = _group;
		}
		
		public function get group():int {
			return __group;
		}
	}
}
