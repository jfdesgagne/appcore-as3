package com.jfdesgagne.appcore.manager.queuegroup.core {
	import flash.events.EventDispatcher;
	import com.jfdesgagne.appcore.debug.dtrace;
	import com.jfdesgagne.appcore.manager.queuegroup.events.QueueGroupEvent;
	import com.jfdesgagne.appcore.manager.queuegroup.group.QueueGroup;
	import com.jfdesgagne.appcore.notifications.Notifications;
	import com.jfdesgagne.appcore.utils.ArrayUtils;

	/**
	 * @author jdesgagne
	 */
	public class QueueGroupManager extends EventDispatcher {
		private static var __instance:QueueGroupManager;
		
		protected var __groups:Array;
		protected var __clonedGroups:Array;
		protected var __currentGroup:QueueGroup;
		protected var __isPaused:Boolean;
		protected var __isNextGroupReady:Boolean;
		
		public function QueueGroupManager() {
			if (__instance == null) {
				init();
			} else {
				throw new Error("[QueueManager] - Can't Instantiate More than Once");
			}
		}

		private function init():void {
			__groups = [];
		}
		
		/**
		 * Returns the instance of the Class (Singleton)
		 * @return The instance of QueueGroupManager 
		 */
		
		public static function getInstance():QueueGroupManager {
			if (__instance == null) __instance = new QueueGroupManager();
			return __instance;
		}
		
		public function addFunction(_fun:Function, _group:int=0, ...args):void {
			var group:QueueGroup = getGroup(_group);
				group.addFunction(_fun, args);
		}
		
		public function removeFunction(_fun:Function, _group:int=0):void {
			var group:QueueGroup = getGroup(_group);
				group.removeFunction(_fun);
		}
		
		
		
		public function start():void {
			if(__groups.length ==0) {
				throw new Error("[QueueGroupManager] - The Queue is empty. Start aborted!");
			} else {
				__clonedGroups = ArrayUtils.clone(__groups);
				executeNextGroup();
			}
		}
		
		public function pause():void {
			__isPaused = true;
			if(__currentGroup) __currentGroup.pause();
		}
		
		public function resume():void {
			if(__groups.length ==0) {
				throw new Error("[QueueGroupManager] - The Queue is empty. Resume aborted!");
			} else {
				__isPaused = false;
				if(__currentGroup) __currentGroup.resume();
				if(__isNextGroupReady) {
					executeNextGroup();
				}
			}
		}
		
		public function reset():void {
			if(__currentGroup) __currentGroup.pause();
			__currentGroup = null;
			__clonedGroups = null;
			__groups = [];
		}
		
		public function functionComplete():void {
			if(__currentGroup) {
				__currentGroup.functionComplete();
			}
		}
		
		private function getGroup(_group:int):QueueGroup {
			if(__groups[_group] == null) {
				__groups[_group] = new QueueGroup(_group);
			}
			
			return QueueGroup(__groups[_group]);
		}
		
		private function executeNextGroup():void {
			__isNextGroupReady = false;
			__currentGroup = __clonedGroups.pop();
			__currentGroup.addEventListener(QueueGroupEvent.GROUP_COMPLETE, onGroupComplete);
			__currentGroup.execute();	
		}

		private function onGroupComplete(_e:QueueGroupEvent):void {
			if(__clonedGroups.length>0) {
				if(__isPaused) {
					__isNextGroupReady = true;
				} else {
					executeNextGroup();
				}
			} else {
				Notifications.notify(new QueueGroupEvent(QueueGroupEvent.ALL_COMPLETE));
				dtrace('all function completed');
			}
		}
	}
}
