package com.jfdesgagne.appcore.manager.queuegroup.group {

	/**
	 * @author jdesgagne
	 */
	public class QueueFunction {
		protected var __fun:Function;
		protected var __args:Array;
		
		public function QueueFunction(_fun:Function, _args:Array):void {
			__fun = _fun;
			if(_args.length>0) __args = _args;
		}
		
		public function get fun():Function {
			return __fun;
		}
		
		public function get args():Array {
			return __args;
		}
		
		public function execute():void {
			if(__args) {
				__fun.apply(null, __args);
			} else {
				__fun();
			}
			
			__fun = null;
			__args = null;
		}
	}
}
