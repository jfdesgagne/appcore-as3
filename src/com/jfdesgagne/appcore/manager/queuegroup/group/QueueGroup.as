package com.jfdesgagne.appcore.manager.queuegroup.group {
	import com.jfdesgagne.appcore.manager.queuegroup.events.QueueGroupEvent;

	import flash.events.EventDispatcher;

	/**
	 * @author jdesgagne
	 */
	public class QueueGroup extends EventDispatcher {
		protected var __pos:int;
		protected var __functions:Array;
		protected var __currentQueueFunction:QueueFunction;
		protected var __run:Boolean;
		protected var __isPaused:Boolean;
		protected var __isNextFunctionReady:Boolean;
		
		public function QueueGroup(_pos:int) {
			__pos = _pos;
			__run = false;
			__isPaused = false;
			__functions = [];
		}
		
		public function addFunction(_fun:Function, args:Array=null):void {
			__functions.push(new QueueFunction(_fun, args));
		}
		
		public function removeFunction(_function:Function):void {
			for(var i:int=__functions.length-1; i>=0; i--) {
				if(QueueFunction(__functions[i]).fun == _function) {
					__functions.splice(i, 1);
				}
			}
		}
		
		public function execute():void {
			gotoNextFunction();
		}
		
		private function gotoNextFunction():void {
			if(__functions.length>0) {
				__currentQueueFunction = __functions.shift();
				__currentQueueFunction.execute();
			} else {
				dispatchEvent(new QueueGroupEvent(QueueGroupEvent.GROUP_COMPLETE, __pos));
			}
		}

		public function functionComplete():void {
			__currentQueueFunction = null;
			
			if(__isPaused) {
				__isNextFunctionReady = true;				
			} else {
				gotoNextFunction();
			}
		}

		public function pause():void {
			__isPaused = true;
		}
		
		public function resume():void {
			__isPaused = false;
			if(__isNextFunctionReady) {
				gotoNextFunction();
			}
		}
	}
}
