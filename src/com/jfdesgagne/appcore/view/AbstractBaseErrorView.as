package com.jfdesgagne.appcore.view {
	import com.jfdesgagne.appcore.statics.ViewTypesStatics;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractBaseErrorView extends AbstractBaseView {
		public function AbstractBaseErrorView():void {
			super();
			__viewType = ViewTypesStatics.TYPE_ERROR;
			__isUnique = false;
		}
		
	}
}
