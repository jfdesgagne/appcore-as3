package com.jfdesgagne.appcore.view {
	import com.jfdesgagne.appcore.statics.ViewTypesStatics;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractBasePromptView extends AbstractBaseView {
		public function AbstractBasePromptView():void {
			super();
			__viewType = ViewTypesStatics.TYPE_PROMPT;
			__isUnique = false;
		}
		
	}
}
