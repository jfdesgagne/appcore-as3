package com.jfdesgagne.appcore.view {
	import com.jfdesgagne.appcore.assets.AppCoreAsset;
	import com.jfdesgagne.appcore.events.ViewEvent;
	import com.jfdesgagne.appcore.notifications.Notifications;
	import com.jfdesgagne.appcore.statics.ViewTypesStatics;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractBaseView extends AppCoreAsset {
		protected var __viewType:int = ViewTypesStatics.TYPE_VIEW;
		protected var __isUnique:Boolean = true;

		public function AbstractBaseView():void {
			super();
		}
		
		
		public function close():void {
			//OVERRIDE IF YOU HAVE A DELAYED TRANSITION OUT
			Notifications.notify(new ViewEvent(ViewEvent.CLOSED, this));
		}
		
		public function initialize(_initParams:Object):void {
			//MUST BE OVERRIDED
		}
		
		public function get viewType():int {
			return __viewType;
		}
		
		public function get isUnique():Boolean {
			return __isUnique;
		}
	}
}
