package com.jfdesgagne.appcore.view {
	import com.jfdesgagne.appcore.statics.ViewTypesStatics;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractBaseModalView extends AbstractBaseView {
		public function AbstractBaseModalView():void {
			super();
			__viewType = ViewTypesStatics.TYPE_MODAL;
			__isUnique = false;
		}
		
	}
}
