package com.jfdesgagne.appcore.assets {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.utils.Dictionary;
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;

	/**
	 * @author jfdesgagne
	 *
	 * based on AssetProxy by Jon Keon (fuel industries)
	 *
	 */

	public dynamic class AppCoreAsset extends AppCoreAssetShortcut {
		protected var __assetId:String;

		public function AppCoreAsset():void {
			if (__assetId != null)
				setDisplay(__assetId);
		}

		public function get display():MovieClip {
			return __display;
		}

		public function setDisplay(_asset:* = null, _addInto:DisplayObjectContainer = null):void {
			if (_asset == null) {
				__display = new MovieClip();
			} else if (_asset is MovieClip) {
				__display = _asset as MovieClip;
			} else if (_asset is String) {
				var instance:Class = Class(getDefinitionByName(_asset));
				__display = new instance() as MovieClip;
			}

			if (!__display)
				throw new Error("[AppCoreAsset] ERROR: Can't set display with " + _asset);
			generateAsset();
			if (_addInto)
				_addInto.addChild(__display);

			assetReady();
		}

		public function assetReady():void {
			//MUST BE OVERRIDED
		}

		private function generateAsset():void {
			var xmlCache:Dictionary = new Dictionary();
			var overriddenVariables:Dictionary;
			var objDescriptor:XML;
			var qualifiedClassName:String = getQualifiedClassName(this);
			var property:XML;
			var name:String;
			var type:String;
			var clazz:Class;
			var inst:*;

			if (xmlCache[qualifiedClassName] is XML) {
				objDescriptor = xmlCache[qualifiedClassName];
			} else {
				objDescriptor = describeType(this);
				xmlCache[qualifiedClassName] = objDescriptor;
			}

			for each (property in objDescriptor..*) {
				if (property.name() == "variable") {
					name = property.@name;
					type = property.@type;

					if (overriddenVariables && overriddenVariables[name]) {
						clazz = overriddenVariables[name];
					} else {
						clazz = getDefinitionByName(type) as Class;
					}

					if (inheritsFrom(clazz, AppCoreAsset)) {
						if (__display[name] != null) {
							if (this[name] == null) {
								inst = new clazz();
							} else {
								inst = this[name];
							}

							AppCoreAsset(inst).setDisplay(__display[name]);

							try {
								this[name] = inst;
							} catch (e:Error) {
								throw new Error("Inheritance error on " + name + ". " + inst + " doesn't extend " + type);
							}
						}
					} else if (type == "flash.display::MovieClip" || type == "flash.text::TextField" || type == "flash.media::Video") {
						this[name] = __display[name];
					} else {
						throw new Error("[AppCoreAsset] - Tried to instantiate a class that derives off of a native flash object (MovieClip, Textfield etc) and is on the display list. No code from class " + type + " will execute. You must make your class extend AppCoreAsset.");
					}
				}
			}
		}

		public function inheritsFrom(descendant:Class, ancestor:Class):Boolean {
			var superName:String;
			var ancestorClassName:String = getQualifiedClassName(ancestor);
			if (ancestorClassName == getQualifiedClassName(descendant))
				return true;
			while (superName != "Object") {
				superName = getQualifiedSuperclassName(descendant);
				descendant = Class(getDefinitionByName(superName));

				if (superName == null)
					return false;
				if (superName == ancestorClassName)
					return true;
			}
			return false;
		}

		public function addDisplayInside(_child:*):void {
			if (_child is DisplayObjectContainer) {
				DisplayObjectContainer(_child).addChild(__display);
			} else if (_child is AppCoreAsset) {
				if (AppCoreAsset(_child).display) {
					AppCoreAsset(_child).display.addChild(__display);
				}
			}
		}

		public function destroy():void {
			//MUST BE OVERRIDED
			if (__display != null) {
				__display.stop();
				__display = null;
			}

		}
	}
}
