package com.jfdesgagne.appcore.assets {
	import flash.accessibility.AccessibilityImplementation;
	import flash.accessibility.AccessibilityProperties;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Scene;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Transform;
	import flash.media.SoundTransform;
	import flash.text.TextSnapshot;
	import flash.ui.ContextMenu;

	/**
	 * @author jfdesgagne
	 */
	public class AppCoreAssetShortcut {
		protected var __display:MovieClip;	
		
		
		public function AppCoreAssetShortcut() {}
		
		////////////////////////////////////////////////////////////////////////////////////
		// MovieClip Functions /////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		public function get currentLabels() : Array {
			return __display.currentLabels;
		}

		public function stop() : void {
			__display.stop();
		}

		public function get currentLabel() : String {
			return __display.currentLabel;
		}

		public function get totalFrames() : int {
			return __display.totalFrames;
		}

		public function prevScene() : void {
			__display.prevScene();
		}

		public function play() : void {
			__display.play();
		}

		public function addFrameScript(...args) : void {
			__display.addFrameScript.apply(__display, args);
		}

		public function nextFrame() : void {
			__display.nextFrame();
		}

		public function get enabled() : Boolean {
			return __display.enabled;
		}

		public function get framesLoaded() : int {
			return __display.framesLoaded;
		}

		public function get scenes() : Array {
			return __display.scenes;
		}

		public function nextScene() : void {
			__display.nextScene();
		}

		public function get currentFrame() : int {
			return __display.currentFrame;
		}

		public function set enabled(value : Boolean) : void {
			__display.enabled = value;
		}

		public function gotoAndStop(frame : Object, scene : String = null) : void {
			if (frame is int) {
				if (this.currentFrame != frame) {
					__display.gotoAndStop(frame, scene);
				}
			}
			else if (frame is String) {
				if (this.currentLabel != frame) {
					__display.gotoAndStop(frame, scene);	
				}	
			}
		}

		public function get currentScene() : Scene {
			return __display.currentScene;
		}

		public function set trackAsMenu(value : Boolean) : void {
			__display.trackAsMenu = value;
		}

		public function gotoAndPlay(frame : Object, scene : String = null) : void {
			__display.gotoAndPlay(frame, scene);
		}

		public function get trackAsMenu() : Boolean {
			return __display.trackAsMenu;
		}

		public function prevFrame() : void {
			__display.prevFrame();
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////
		// Sprite Functions ////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		public function get soundTransform() : SoundTransform {
			return __display.soundTransform;
		}

		public function set useHandCursor(value : Boolean) : void {
			__display.useHandCursor = value;
		}

		public function set soundTransform(sndTransform : SoundTransform) : void {
			__display.soundTransform = sndTransform;
		}

		public function stopDrag() : void {
			__display.stopDrag();
		}

		public function get dropTarget() : DisplayObject {
			return __display.dropTarget;
		}

		public function set hitArea(value : Sprite) : void {
			__display.hitArea = value;
		}

		public function get graphics() : Graphics {
			return __display.graphics;
		}

		public function get useHandCursor() : Boolean {
			return __display.useHandCursor;
		}

		public function startDrag(lockCenter : Boolean = false, bounds : Rectangle = null) : void {
			__display.startDrag(lockCenter, bounds);
		}

		public function get hitArea() : Sprite {
			return __display.hitArea;
		}

		public function set buttonMode(value : Boolean) : void {
			__display.buttonMode = value;
		}

		public function get buttonMode() : Boolean {
			return __display.buttonMode;
		}
		
		////////////////////////////////////////////////////////////////////////////////////
		// Display Object Container Functions //////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		public function addChild(child : DisplayObject) : DisplayObject {
			return __display.addChild(child);
		}

		public function getChildByName(name : String) : DisplayObject {
			return __display.getChildByName(name);
		}

		public function get textSnapshot() : TextSnapshot {
			return __display.textSnapshot;
		}

		public function getChildIndex(child : DisplayObject) : int {
			return __display.getChildIndex(child);
		}

		public function set mouseChildren(enable : Boolean) : void {
			__display.mouseChildren = enable;
		}

		public function setChildIndex(child : DisplayObject, index : int) : void {
			__display.setChildIndex(child, index);
		}

		public function addChildAt(child : DisplayObject, index : int) : DisplayObject {
			return __display.addChildAt(child, index);
		}

		public function contains(child : DisplayObject) : Boolean {
			return __display.contains(child);
		}

		public function get numChildren() : int {
			return __display.numChildren;
		}

		public function swapChildrenAt(index1 : int, index2 : int) : void {
			__display.swapChildrenAt(index1, index2);
		}

		public function get tabChildren() : Boolean {
			return __display.tabChildren;
		}

		public function getChildAt(index : int) : DisplayObject {
			return __display.getChildAt(index);
		}

		public function swapChildren(child1 : DisplayObject, child2 : DisplayObject) : void {
			__display.swapChildren(child1, child2);
		}

		public function getObjectsUnderPoint(point : Point) : Array {
			return __display.getObjectsUnderPoint(point);
		}

		public function get mouseChildren() : Boolean {
			return __display.mouseChildren;
		}

		public function removeChildAt(index : int) : DisplayObject {
			return __display.removeChildAt(index);
		}

		public function set tabChildren(enable : Boolean) : void {
			__display.tabChildren = enable;
		}

		public function areInaccessibleObjectsUnderPoint(point : Point) : Boolean {
			return __display.areInaccessibleObjectsUnderPoint(point);
		}

		public function removeChild(child : DisplayObject) : DisplayObject {
			return __display.removeChild(child);
		}
		
		////////////////////////////////////////////////////////////////////////////////////
		// Interactive Object Functions ////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		public function get tabEnabled() : Boolean {
			return __display.tabEnabled;
		}

		public function get doubleClickEnabled() : Boolean {
			return __display.doubleClickEnabled;
		}

		public function set contextMenu(cm : ContextMenu) : void {
			__display.contextMenu = cm;
		}

		public function get accessibilityImplementation() : AccessibilityImplementation {
			return __display.accessibilityImplementation;
		}

		public function set doubleClickEnabled(enabled : Boolean) : void {
			__display.doubleClickEnabled = enabled;
		}

		public function get contextMenu() : ContextMenu {
			return __display.contextMenu;
		}

		public function get mouseEnabled() : Boolean {
			return __display.mouseEnabled;
		}

		public function set focusRect(focusRect : Object) : void {
			__display.focusRect = focusRect;
		}

		public function get tabIndex() : int {
			return __display.tabIndex;
		}

		public function set mouseEnabled(enabled : Boolean) : void {
			__display.mouseEnabled = enabled;
		}

		public function get focusRect() : Object {
			return __display.focusRect;
		}

		public function set tabEnabled(enabled : Boolean) : void {
			__display.tabEnabled = enabled;
		}

		public function set accessibilityImplementation(value : AccessibilityImplementation) : void {
			__display.accessibilityImplementation = value;
		}

		public function set tabIndex(index : int) : void {
			__display.tabIndex = index;
		}
		
		////////////////////////////////////////////////////////////////////////////////////
		// Display Object Functions ////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		public function get y() : Number {
			return __display.y;
		}

		public function get transform() : Transform {
			return __display.transform;
		}

		public function get stage() : Stage {
			return __display.stage;
		}

		public function localToGlobal(point : Point) : Point {
			return __display.localToGlobal(point);
		}

		public function get name() : String {
			return __display.name;
		}

		public function set width(value : Number) : void {
			__display.width = value;
		}

		public function get blendMode() : String {
			return __display.blendMode;
		}

		public function get scale9Grid() : Rectangle {
			return __display.scale9Grid;
		}

		public function set name(value : String) : void {
			__display.name = value;
		}

		public function set scaleX(value : Number) : void {
			__display.scaleX = value;
		}

		public function set scaleY(value : Number) : void {
			__display.scaleY = value;
		}

		public function get accessibilityProperties() : AccessibilityProperties {
			return __display.accessibilityProperties;
		}

		public function set scrollRect(value : Rectangle) : void {
			__display.scrollRect = value;
		}

		public function get cacheAsBitmap() : Boolean {
			return __display.cacheAsBitmap;
		}

		public function globalToLocal(point : Point) : Point {
			return __display.globalToLocal(point);
		}

		public function get height() : Number {
			return __display.height;
		}

		public function set blendMode(value : String) : void {
			__display.blendMode = value;
		}

		public function get parent() : DisplayObjectContainer {
			return __display.parent;
		}

		public function getBounds(targetCoordinateSpace : DisplayObject) : Rectangle {
			return __display.getBounds(targetCoordinateSpace);
		}

		public function get opaqueBackground() : Object {
			return __display.opaqueBackground;
		}

		public function set scale9Grid(innerRectangle : Rectangle) : void {
			__display.scale9Grid = innerRectangle;
		}

		public function set alpha(value : Number) : void {
			__display.alpha = value;
		}

		public function set accessibilityProperties(value : AccessibilityProperties) : void {
			__display.accessibilityProperties = value;
		}

		public function get width() : Number {
			return __display.width;
		}

		public function hitTestPoint(x : Number, y : Number, shapeFlag : Boolean = false) : Boolean {
			return __display.hitTestPoint(x, y, shapeFlag);
		}

		public function get scaleX() : Number {
			return __display.scaleX;
		}

		public function get scaleY() : Number {
			return __display.scaleY;
		}

		public function get mouseX() : Number {
			return __display.mouseX;
		}

		public function set height(value : Number) : void {
			__display.height = value;
		}

		public function set mask(value : DisplayObject) : void {
			__display.mask = value;
		}

		public function getRect(targetCoordinateSpace : DisplayObject) : Rectangle {
			return __display.getRect(targetCoordinateSpace);
		}

		public function get mouseY() : Number {
			return __display.mouseY;
		}

		public function get alpha() : Number {
			return __display.alpha;
		}

		public function set transform(value : Transform) : void {
			__display.transform = value;
		}

		public function get scrollRect() : Rectangle {
			return __display.scrollRect;
		}

		public function get loaderInfo() : LoaderInfo {
			return __display.loaderInfo;
		}

		public function get root() : DisplayObject {
			return __display.root;
		}

		public function set visible(value : Boolean) : void {
			__display.visible = value;
		}

		public function set opaqueBackground(value : Object) : void {
			__display.opaqueBackground = value;
		}

		public function set cacheAsBitmap(value : Boolean) : void {
			__display.cacheAsBitmap = value;
		}

		public function hitTestObject(obj : DisplayObject) : Boolean {
			return __display.hitTestObject(obj);
		}

		public function set x(value : Number) : void {
			__display.x = value;
		}

		public function set y(value : Number) : void {
			__display.y = value;
		}

		public function get mask() : DisplayObject {
			return __display.mask;
		}

		public function set filters(value : Array) : void {
			__display.filters = value;
		}

		public function get x() : Number {
			return __display.x;
		}

		public function get visible() : Boolean {
			return __display.visible;
		}

		public function get filters() : Array {
			return __display.filters;
		}

		public function set rotation(value : Number) : void {
			__display.rotation = value;
		}

		public function get rotation() : Number {
			return __display.rotation;
		}
		
		////////////////////////////////////////////////////////////////////////////////////
		// Event Dispatcher Functions //////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		public function dispatchEvent(event : Event) : Boolean {

			return __display.dispatchEvent(event);
		}

		public function willTrigger(type : String) : Boolean {
			return __display.willTrigger(type);
		}

//		public function toString() : String {
//			return __display.toString();
//		}

		public function removeEventListener(type : String, listener : Function, useCapture : Boolean = false) : void {
			__display.removeEventListener(type, listener, useCapture);
		}

		public function hasEventListener(type : String) : Boolean {
			return __display.hasEventListener(type);
		}

		public function addEventListener(type : String, listener : Function, useCapture : Boolean = false, priority : int = 0, useWeakReference : Boolean = true) : void {
			__display.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
	}
}
