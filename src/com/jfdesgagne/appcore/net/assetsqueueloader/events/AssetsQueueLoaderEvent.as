package com.jfdesgagne.appcore.net.assetsqueueloader.events {
	import flash.events.Event;

	public class AssetsQueueLoaderEvent extends Event {
		public static const LOADING_ASSET_PROGRESS:String = "AssetsQueueLoaderEvent_LOADING_ASSET_PROGRESS";		public static const LOADING_ASSET_COMPLETE:String = "AssetsQueueLoaderEvent_LOADING_ASSET_COMPLETE";		public static const LOADING_PROGRESS:String = "AssetsQueueLoaderEvent_LOADING_PROGRESS";		public static const LOADING_COMPLETE:String = "AssetsQueueLoaderEvent_LOADING_COMPLETE";		
		private var __bytesLoaded:int;
		private var __bytesTotal:int;
		
		public function AssetsQueueLoaderEvent(type : String, _bytesLoaded:int=0, _bytesTotal:int=0, bubbles : Boolean = true, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
			__bytesLoaded = _bytesLoaded;
			__bytesTotal = _bytesTotal;
		}
		
		public function get bytesLoaded():int {
			return __bytesLoaded;	
		}
		public function get bytesTotal():int {
			return __bytesTotal;	
		}
	}
}
