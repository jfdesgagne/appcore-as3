package com.jfdesgagne.appcore.net.assetsqueueloader.core {
	import com.jfdesgagne.appcore.net.assetsqueueloader.asset.Asset;
	import com.jfdesgagne.appcore.net.assetsqueueloader.events.AssetsQueueLoaderEvent;

	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;

	/**
	 * @author jfdesgagne
	 */
	public class AssetsQueueLoader extends EventDispatcher {
		//Singleton Instance
		private static var __instance:AssetsQueueLoader;
		
		//Privates variables
		private var __queue:Array;
		private var __tempAsset:Asset;
		private var __loaderContext:LoaderContext;
		
		public function AssetsQueueLoader(target : IEventDispatcher = null) {
			if (__instance == null) {
				super(target);
				init();
			} else {
				throw (new Error("[AssetQueueLoader] - Can't instantiate more than once"));
			}
		}

		private function init() : void {
			__queue = [];
			__loaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
		}

		public static function getInstance():AssetsQueueLoader {
			if (__instance == null) {
				__instance = new AssetsQueueLoader();
			}
			return __instance;
		}
		
		public function addAsset(_path:String):void {
			if(__queue.indexOf(_path) == -1) {
				__queue.push(_path);
			} else {
				throw (new Error("[AssetQueueLoader :: addAsset] - The asset "+_path+" already exist in queue"));
			}
		}
		
		public function load():void {
			if(__queue.length > 0) {
				loadNextAsset();
			} else {
				throw (new Error("[AssetQueueLoader :: load] - The queue is empty"));
			}
		}
		
		public function pause():void {
			__tempAsset.removeEventListener(AssetsQueueLoaderEvent.LOADING_ASSET_COMPLETE, onAssetComplete);
			__tempAsset.removeEventListener(AssetsQueueLoaderEvent.LOADING_ASSET_PROGRESS, onAssetProgress);
		}
		
		protected function loadNextAsset():void {
			if(__tempAsset) {
				__tempAsset.removeEventListener(AssetsQueueLoaderEvent.LOADING_ASSET_COMPLETE, onAssetComplete);
				__tempAsset.removeEventListener(AssetsQueueLoaderEvent.LOADING_ASSET_PROGRESS, onAssetProgress);
			}
			
			__tempAsset = new Asset(__queue.pop(), __loaderContext);
			__tempAsset.addEventListener(AssetsQueueLoaderEvent.LOADING_ASSET_COMPLETE, onAssetComplete);			__tempAsset.addEventListener(AssetsQueueLoaderEvent.LOADING_ASSET_PROGRESS, onAssetProgress);
		}
		
		protected function onAssetComplete(e:AssetsQueueLoaderEvent):void {
			e.stopImmediatePropagation();
			dispatchEvent(new AssetsQueueLoaderEvent(AssetsQueueLoaderEvent.LOADING_ASSET_COMPLETE));
		
			if(__queue.length == 0) {
				dispatchEvent(new AssetsQueueLoaderEvent(AssetsQueueLoaderEvent.LOADING_COMPLETE));
			} else {
				loadNextAsset();
			}
		}

		protected function onAssetProgress(e:AssetsQueueLoaderEvent):void {
			e.stopImmediatePropagation();
			dispatchEvent(new AssetsQueueLoaderEvent(AssetsQueueLoaderEvent.LOADING_ASSET_PROGRESS, e.bytesLoaded, e.bytesTotal));
		}
	}
}
