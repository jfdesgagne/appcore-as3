package com.jfdesgagne.appcore.net.assetsqueueloader.asset {
	import flash.system.LoaderContext;
	import com.jfdesgagne.appcore.net.assetsqueueloader.events.AssetsQueueLoaderEvent;

	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;

	/**
	 * @author jfdesgagne
	 */
	public class Asset extends EventDispatcher {
		private var __path:String;
		private var __loader:Loader;
		private var __isLoaded:Boolean = false;
		
		public function Asset(_path:String, _loaderContext:LoaderContext) {
			__path = _path;
			
			__loader = new Loader();
			__loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			__loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			__loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			__loader.contentLoaderInfo.addEventListener(IOErrorEvent.NETWORK_ERROR, onIONetworkError);
			__loader.contentLoaderInfo.addEventListener(IOErrorEvent.DISK_ERROR, onIODiskError);
			__loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			__loader.load(new URLRequest(_path), _loaderContext);
		}
		
		protected function onComplete(e:Event):void {
			__loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
			__loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
			__loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			__loader.contentLoaderInfo.removeEventListener(IOErrorEvent.NETWORK_ERROR, onIONetworkError);
			__loader.contentLoaderInfo.removeEventListener(IOErrorEvent.DISK_ERROR, onIODiskError);
			__loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
		
			__isLoaded = true;
			
			dispatchEvent(new AssetsQueueLoaderEvent(AssetsQueueLoaderEvent.LOADING_ASSET_COMPLETE));
		}
		
		protected function onProgress(e:ProgressEvent):void {
			dispatchEvent(new AssetsQueueLoaderEvent(AssetsQueueLoaderEvent.LOADING_ASSET_PROGRESS, e.bytesLoaded, e.bytesTotal));
		}
		
		protected function onIOError(e:IOErrorEvent):void {
			throw (new Error("[Asset "+__path+"] - IOError " + e));
		}
		
		protected function onIONetworkError(e:IOErrorEvent):void {
			throw (new Error("[Asset " +__path+"] - IONetworkError " + e));
		}
		
		protected function onIODiskError(e:IOErrorEvent):void {
			throw (new Error("[Asset "+__path+"] - IODiskError " + e));
		}
		
		protected function onSecurityError(e:SecurityErrorEvent):void {
			throw (new Error("[Asset "+__path+"] - SecurityError " + e));
		}
		
		public function get content() : DisplayObject {
			return __loader.content;
		}
	}
}
