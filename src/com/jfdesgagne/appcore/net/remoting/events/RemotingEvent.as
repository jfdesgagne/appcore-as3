package com.jfdesgagne.appcore.net.remoting.events {
	import com.jfdesgagne.appcore.net.remoting.services.AbstractService;

	import flash.events.Event;

	/**
	 * @author jfdesgagne
	 */
	public class RemotingEvent extends Event {
		public static const SUCCESS:String = "RemotingSuccessEvent_SUCCESS";		public static const FAIL:String = "RemotingSuccessEvent_FAIL";		public static const DESTROY:String = "RemotingSuccessEvent_DESTROY";
		private var __service:AbstractService;
		
		public function RemotingEvent(_type:String, _service:AbstractService, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(_type, bubbles, cancelable);
			__service = _service;
		}
		
		public function get service():AbstractService {
			return __service;
		}
		
		public function destroy():void {
			__service = null;
		}
	}
}
