package com.jfdesgagne.appcore.net.remoting.statics {

	/**
	 * @author jfdesgagne
	 */
	public class RemotingConfigurationStatics {
		public static var AMF_SERVER:String = "http://localhost/";		public static var SMARTFOX_SERVER:String = "127.0.0.1";		public static var SMARTFOX_PORT:int = 9339;		public static var SMARTFOX_USER:String = "";		public static var SMARTFOX_PASS:String = "";
		
		public function RemotingConfigurationStatics():void {
			
		}
	}
}
