package com.jfdesgagne.appcore.net.remoting.statics {

	/**
	 * @author jfdesgagne
	 */
	public class RemotingErrorStatics {
		
		public static const ERROR_NO_ERROR:String = "No Error";
		public static const ERROR_AMF_SECURITY:String = "AMFService Security Error";
		public static const ERROR_AMF_IO:String = "AMFService IO Error";		public static const ERROR_SMARTFOX_CONNECTION:String = "Error connecting to SmartFox server";
		
		public function RemotingErrorStatics() {}
	}
}
