package com.jfdesgagne.appcore.net.remoting.core {
	import com.jfdesgagne.appcore.net.remoting.events.RemotingEvent;
	import com.jfdesgagne.appcore.net.remoting.services.AbstractService;

	/**
	 * @author jfdesgagne
	 * 
	 * Used to manage services reference 
	 * (to avoid loose them by the garbage collector during the call)
	 * 
	 */
	public class RemotingManager {
		private static var __instance:RemotingManager;
		protected var __services:Array;
		
		public function RemotingManager() {
			if (__instance == null) {
				init();
			} else {
				throw new Error("[RemotingManager] - Can't Instantiate More than Once");
			}
		}
		
		public static function getInstance():RemotingManager {
			if (__instance == null) __instance = new RemotingManager();
			return __instance;
		}
		
		protected function init():void {
			__services = [];
		}
		
		public function startService(_service:Class, _onCompleteCallback:Function=null, _onFailCallback:Function=null):void {
			var services:AbstractService = new _service(_onCompleteCallback, _onFailCallback);
				services.addEventListener(RemotingEvent.DESTROY, onServiceDestroy);
				services.start();
			__services.push(services);
		}

		private function onServiceDestroy(_e:RemotingEvent):void {
			var index:int = __services.indexOf(_e.service);
			if(index > -1) __services.splice(index, 1);
		}
	}
}
