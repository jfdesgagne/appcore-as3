package com.jfdesgagne.appcore.net.remoting.services {
	import com.jfdesgagne.appcore.net.remoting.events.RemotingEvent;

	import flash.events.EventDispatcher;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractService extends EventDispatcher {
		protected var __error:String;
		protected var __data:*;		protected var __rawData:Array;
		
		public function AbstractService(_onCompleteCallback:Function=null, _onFailCallback:Function=null):void {
			if(_onCompleteCallback!=null) addEventListener(RemotingEvent.SUCCESS, _onCompleteCallback);
			if(_onFailCallback!=null) addEventListener(RemotingEvent.FAIL, _onFailCallback);
		}
		
		public function call(...args):void {
			
		}
		
		public function start():void {
			
		}
		
		protected function parseData(...args):void {
			//MUST BE OVERRIDED	
		}
		
		protected function onServiceComplete(...args):void {
			__rawData = args;
			parseData.apply(null, args);
			dispatchEvent(new RemotingEvent(RemotingEvent.SUCCESS, this));
		}
		
		protected function onServiceFail():void {
			dispatchEvent(new RemotingEvent(RemotingEvent.FAIL, this));
		}
		
		public function get error():String {
			return __error;
		}

		public function destroy():void {
			dispatchEvent(new RemotingEvent(RemotingEvent.DESTROY, this));
		}
	}
}
