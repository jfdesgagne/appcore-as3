package com.jfdesgagne.appcore.net.remoting.services {
	import it.gotoandplay.smartfoxserver.SFSEvent;
	import it.gotoandplay.smartfoxserver.SmartFoxClient;

	import com.jfdesgagne.appcore.debug.dtrace;
	import com.jfdesgagne.appcore.net.remoting.statics.RemotingConfigurationStatics;
	import com.jfdesgagne.appcore.net.remoting.statics.RemotingErrorStatics;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractSmartFoxService extends AbstractService {
		public static var smartFox:SmartFoxClient;
		protected var __callArgs:Array;
		protected var __zone:String;

		public function AbstractSmartFoxService(_onCompleteCallback:Function = null, _onFailCallback:Function = null):void {
			super(_onCompleteCallback, _onFailCallback);
		}

		override public function call(...args):void {
			__callArgs = args;
			if(smartFox == null) {
				connectSmartFoxServer();
			} else {
				if(__zone != __callArgs[0]) {
					login();
				} else {
					if(__callArgs.length==3) {
						smartFox.sendXtMessage(__callArgs[1], "", __callArgs[2]);
					} else if(__callArgs.length==4) {
						smartFox.sendXtMessage(__callArgs[1], __callArgs[2], __callArgs[3]);
					} else {
						throw new Error("[SmartFoxServer] Invalid number of argument!");
					}
					__callArgs = null;
				}
			}
		}

		protected function connectSmartFoxServer():void {
			smartFox = new SmartFoxClient();
			smartFox.addEventListener(SFSEvent.onConnection, onConnectionHandler);
			smartFox.addEventListener(SFSEvent.onExtensionResponse, onExtensionResponseHandler);
			smartFox.addEventListener(SFSEvent.onRoomListUpdate, onRoomListUpdate);
			smartFox.addEventListener(SFSEvent.onJoinRoom, onJoinRoom);			smartFox.addEventListener(SFSEvent.onAdminMessage, onAdminMessage);
			smartFox.connect(RemotingConfigurationStatics.SMARTFOX_SERVER, RemotingConfigurationStatics.SMARTFOX_PORT);
		}

		protected function login():void {
			__zone = __callArgs[0];
			smartFox.login(__zone, RemotingConfigurationStatics.SMARTFOX_USER, RemotingConfigurationStatics.SMARTFOX_PASS);
		}

		private static function onAdminMessage(_e:SFSEvent):void {
			dtrace('[SmartFoxServer] Admin Message Recived: ' + _e.params["message"]);
		}

		protected function onRoomListUpdate(_e:SFSEvent):void {
			smartFox.autoJoin();
		}

		protected function onJoinRoom(_e:SFSEvent):void {
			if(__callArgs) call.apply(null, __callArgs);
		}

		private function onExtensionResponseHandler(_e:SFSEvent):void {
			onServiceComplete(_e.params["dataObj"]);
		}

		protected function onConnectionHandler(_e:SFSEvent):void {
			dtrace('SmartFoxClient connected');
			if (_e.params["success"]) {
				login();
			} else {
				__error = RemotingErrorStatics.ERROR_SMARTFOX_CONNECTION;
				onServiceFail();
			}	
		}
	}		
}
