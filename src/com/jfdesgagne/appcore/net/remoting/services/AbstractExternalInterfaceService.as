package com.jfdesgagne.appcore.net.remoting.services {
	import flash.external.ExternalInterface;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractExternalInterfaceService extends AbstractService {
		public function AbstractExternalInterfaceService(_onCompleteCallback:Function=null, _onFailCallback:Function=null):void {
			super(_onCompleteCallback, _onFailCallback);
		}
		
		override public function call(...args):void {
			ExternalInterface.addCallback(args[0], onServiceComplete);
			ExternalInterface.call.apply(null, args);
		}	

	}
}
