package com.jfdesgagne.appcore.net.remoting.services {
	import com.jfdesgagne.appcore.net.remoting.statics.RemotingErrorStatics;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	/**
	 * @author jfdesgagne
	 */
	public class AbstractAMFService extends AbstractService {
		public static var PATH:String;
		
		protected var __path:String;
		protected var __urlLoader:URLLoader;
		protected var __urlVariables:URLVariables;
		protected var __urlRequest:URLRequest;
		
		public function AbstractAMFService(_onCompleteCallback:Function=null, _onFailCallback:Function=null, _path:String=""):void {
			__path = PATH + _path;
			__urlLoader = new URLLoader();
			__urlLoader.addEventListener(Event.COMPLETE, onLoaderComplete);
			__urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoaderSecurityError);
			__urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoaderIOError);
			__urlRequest = new URLRequest(__path);
			__urlRequest.method = URLRequestMethod.POST;
			
			super(_onCompleteCallback, _onFailCallback);
		}
		
		protected function onLoaderComplete(_e:Event):void {
			onServiceComplete(__urlLoader.data);
		}
		
		protected function onLoaderSecurityError(_e:SecurityErrorEvent):void {
			__error = RemotingErrorStatics.ERROR_AMF_SECURITY;
			onServiceFail();
		}
		
		protected function onLoaderIOError(_e:IOErrorEvent):void {
			trace(_e.text);
			__error = RemotingErrorStatics.ERROR_AMF_IO;
			onServiceFail();
		}

		override public function call(...args):void {
			buildVariables(args[1]);
			__urlRequest.url = __path + args[0];
			__urlLoader.load(__urlRequest);
		}
		
		protected function buildVariables(_args:Object):void {
			var urlVariables:URLVariables = new URLVariables();
			for(var element:String in _args) {
				urlVariables[element] = _args[element];
			}
			
			__urlRequest.data = urlVariables;
		}
	}
}
