package com.jfdesgagne.appcore.core {
	import com.jfdesgagne.appcore.data.environment.AppEnvironment;
	import com.jfdesgagne.appcore.debug.Stats;
	import com.jfdesgagne.appcore.manager.view.ViewManager;
	import com.jfdesgagne.appcore.net.assetsqueueloader.core.AssetsQueueLoader;
	import com.jfdesgagne.appcore.net.assetsqueueloader.events.AssetsQueueLoaderEvent;

	import flash.display.Sprite;
	import flash.display.Stage;

	/**
	 * @author jfdesgagne
	 */
	public class AppCore extends Sprite {
		public static var stageRef:Stage;
		protected var __assetQueueLoader:AssetsQueueLoader;		protected var __viewManager:ViewManager;
		protected var __stats:Stats;
		
		public function AppCore() {
			//set variables
			__assetQueueLoader = AssetsQueueLoader.getInstance();
			__viewManager = ViewManager.getInstance();
			stageRef = stage;
			
			//if debug mode, show stats
			if(AppEnvironment.DEBUG_MODE) {
				__stats = new Stats();
				addChild(__stats);
			}
			
			//add view manager to stage
			addChild(__viewManager);
		}
		
		protected function loadAssets():void {
			__assetQueueLoader.addEventListener(AssetsQueueLoaderEvent.LOADING_COMPLETE, onLoadComplete );
			__assetQueueLoader.load();
		}
		
		protected function onLoadComplete(e:AssetsQueueLoaderEvent) : void {
			//MUST BE OVERRIDED
		}
		
	}
}
