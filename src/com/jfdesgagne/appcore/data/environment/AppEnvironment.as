package com.jfdesgagne.appcore.data.environment {
	import com.jfdesgagne.appcore.debug.dtrace;

	public class AppEnvironment {
		
		public static var CURRENT_SERVER:int;
		public static const LOCAL:int = 0;
		public static const LIVE:int = 1;
		

		public static function setEnvironment(rootURL : String) : void {
			if(new RegExp("file://").test(rootURL)) {
				CURRENT_SERVER = LOCAL;
			} else {
				CURRENT_SERVER = LIVE;
			}
			
			dtrace("AppEnvironment.setEnvironment("+CURRENT_SERVER+")");
		}

		
		public static function get DEBUG_MODE() : Boolean {
			return CURRENT_SERVER == LOCAL;
		}
	}
}
