package com.jfdesgagne.appcore.data.binding {
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;

	/**
	 * @author jfdesgagne
	 */
	public class BindableData extends EventDispatcher {
		protected var __props:Dictionary;
		
		public function BindableData(target : IEventDispatcher = null):void {
			super(target);
			__props = new Dictionary(true);
		}
		
		protected function get(_prop:String) : * {
			return getBindedData(_prop).data;
		}
		
		protected function set(_prop:String, _newValue:*) : void {
			getBindedData(_prop).data = _newValue;
		}
		
		private function getBindedData(_prop:String):BindedData {
			if(__props[_prop] == null) __props[_prop] = new BindedData(_prop);
			return __props[_prop];
		}
		
		public function bind(_prop:String, _callback:Function):void {
			getBindedData(_prop).addDispatcher(_callback);
		}
		
		public function unbind(_prop:String, _callback:Function):void {
			getBindedData(_prop).removeDispatcher(_callback);
		}
		
		public function resetBindings(_prop:String):void {
			getBindedData(_prop).removeAllDispatcher();
		}
		
		public function resetAllBindings():void {
			for each(var prop:String in __props) {
				resetBindings(prop);
			}
		}
		
		public function destroy():void {
			resetAllBindings();	
		}
	}
}
