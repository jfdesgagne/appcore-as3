package com.jfdesgagne.appcore.data.binding {
	import flash.utils.Dictionary;

	/**
	 * @author jfdesgagne
	 */
	 
	public class BindedData {
		protected var __name:String;
		protected var __data:*;
		protected var __dispatchers:Dictionary;
		
		public function BindedData(_name:String, _data:*=null):void {
			__name = _name;
			__dispatchers = new Dictionary(true);
			if(_data) data = _data;
		}
		
		public function get data() : * {
			return __data;
		}
		
		public function set data(_data : *) : void {
			var oldValue:* = __data;
			__data = _data;
			dispatch(oldValue);
		}

		private function dispatch(_oldValue:*):void {
			for each(var dispatchFunction:Function in __dispatchers) {
				try { 
					dispatchFunction(_oldValue, __data); 
				}	catch (e1 : ArgumentError) {
					try { 
						dispatchFunction(__data); 
					} catch (e2 : ArgumentError) {
						try { 
							dispatchFunction();
						} catch (e3 : ArgumentError) {
							trace( '[Binding Error] Unable to dispatch property '+__name+" on "+dispatchFunction);
						}
					}
				}
			}
		}

		public function addDispatcher(_function:Function):void {
			__dispatchers[_function] = _function;
		}
		
		public function removeDispatcher(_function:Function):void {
			delete __dispatchers[_function];
		}

		public function removeAllDispatcher() : void {
			for each(var dispatchFunction:Function in __dispatchers) {
				delete __dispatchers[dispatchFunction];
			}
			
			__dispatchers = new Dictionary(true);
		}
	}
}
