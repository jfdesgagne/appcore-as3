package com.jfdesgagne.appcore.data.globals {
	import com.jfdesgagne.appcore.core.AppCore;

	import flash.display.LoaderInfo;
	import flash.net.SharedObject;
	import flash.utils.Dictionary;

	/**
	 * @author jfdesgagne
	 */
	public class GlobalsData {
		private static var __instance:GlobalsData;
		
		public static const STATICS:String = "GlobalsData_STATICS";		public static const FLASH_VARS:String = "GlobalsData_FLASH_VARS";		public static const SHARED_OBJECT:String = "GlobalsData_SHARED_OBJECT";
		
		private var __statics:Dictionary;
		private var __flashVars:Object;
		private var __sharedObject:SharedObject;
		
		public function GlobalsData() {
			if (__instance == null) {
				init();
			} else {
				throw new Error("[GlobalsData] - Can't Instantiate More than Once");
			}
		}
		
		public static function getInstance():GlobalsData {
			if (__instance == null) __instance = new GlobalsData();
			return __instance;
		}

		private function init():void {
			initData(STATICS);
			initData(FLASH_VARS);
			initData(SHARED_OBJECT);
		}
		
		public function set(_name:String, _value:*, _type:String=STATICS):void {
			switch(_type) {
				case STATICS:
					__statics[_name] = _value;
					break;
				case FLASH_VARS:
					throw new Error("[GlobalsData] - Can't add data to a FlashVars");
					break;
				case SHARED_OBJECT:
					__sharedObject.data[_name] = _value;
					__sharedObject.flush();
					break;
			}
		}
		
		public function get(_name:String, _type:String=STATICS):* {
			switch(_type) {
				case STATICS:
					return __statics[_name];
				case FLASH_VARS:
					return __flashVars[_name];
				case SHARED_OBJECT:
					return __sharedObject.data[_name];
			}					
		}
		
		public function remove(_name:String, _type:String=STATICS):void {
			switch(_type) {
				case STATICS:
					delete __statics[_name];
					break;
				case FLASH_VARS:
					throw new Error("[GlobalsData] - Can't remove data from a FlashVars");
					break;
				case SHARED_OBJECT:
					delete __sharedObject.data[_name];
			}
		}
		
		public function reset(_type:String=STATICS):void {
			switch(_type) {
				case STATICS:
					__statics = new Dictionary(true);
					break;
				case FLASH_VARS:
					throw new Error("[GlobalsData] - Can't reset a FlashVars");
					break;
				case SHARED_OBJECT:
					__sharedObject.clear();
					__sharedObject.flush();
					break;
			}
		}
		
		protected function initData(_type:String):void {
			switch(_type) {
				case STATICS:
					reset(STATICS);
					break;
				case FLASH_VARS:
					__flashVars = LoaderInfo(AppCore.stageRef.root.loaderInfo).parameters;
					break;
				case SHARED_OBJECT:
					__sharedObject = SharedObject.getLocal("userData");
					break;
			}
		}
	}
}
