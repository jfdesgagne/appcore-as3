package com.jfdesgagne.appcore_example.data {
	import com.jfdesgagne.appcore.data.binding.BindableData;

	import flash.events.IEventDispatcher;

	/**
	 * @author jfdesgagne
	 */
	public class TestData extends BindableData {
		public function TestData(target : IEventDispatcher = null) {
			super(target);
		}
		
		public function set testString(_val:String):void {
			set('testString', _val);
		}
		
		public function get testString():String {
			return get('testString');
		}
	}
}
