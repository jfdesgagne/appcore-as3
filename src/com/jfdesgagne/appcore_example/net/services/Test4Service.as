package com.jfdesgagne.appcore_example.net.services {
	import com.jfdesgagne.appcore.net.remoting.services.AbstractSmartFoxService;

	/**
	 * @author jfdesgagne
	 */
	public class Test4Service extends AbstractSmartFoxService {
		protected var __receivedData:Object;
		
		public function Test4Service(_onCompleteCallback:Function=null, _onFailCallback:Function=null):void {
			super(_onCompleteCallback, _onFailCallback);
		}
		
		override public function start():void {
			
//			call("appcore_example", "testService", {lala:"test"});
		}

		override protected function parseData(...args):void {
			__receivedData = args[0];
			super.parseData(args);
		}
		
			public function get receivedData():Object {
			return __receivedData;
		}
	}
}
