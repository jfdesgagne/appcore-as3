package com.jfdesgagne.appcore_example.net.services {
	import com.jfdesgagne.appcore.net.remoting.services.AbstractExternalInterfaceService;

	/**
	 * @author jfdesgagne
	 */
	public class TestService extends AbstractExternalInterfaceService {
		protected var __receivedTest1:String;		protected var __receivedTest2:String;
		
		public function TestService(_onCompleteCallback:Function=null, _onFailCallback:Function=null):void {
			super(_onCompleteCallback, _onFailCallback);
		}

		override public function start():void {
			call("testService", "salut", "grace");
		}

		override protected function parseData(...args):void {
			__receivedTest1 = args[0];
			__receivedTest2 = args[1];
		}

		
		public function get receivedTest1():String {
			return __receivedTest1;
		}
		
		public function get receivedTest2():String {
			return __receivedTest2;
		}
	}
}
