package com.jfdesgagne.appcore_example.net.services {
	import com.jfdesgagne.appcore.net.remoting.services.AbstractAMFService;

	/**
	 * @author jfdesgagne
	 */
	public class Test2Service extends AbstractAMFService {
		protected var __receivedText:String;
		
		public function Test2Service(_onCompleteCallback:Function=null, _onFailCallback:Function=null):void {
			super(_onCompleteCallback, _onFailCallback);
		}

		override public function start():void {
			call("testService.php", {data1:"salut", data2:"Grace"});
		}

		override protected function parseData(...args):void {
			__receivedText = args[0];
			super.parseData(args);
		}
		
		public function get receivedText():String {
			return __receivedText;
		}
	}
}
