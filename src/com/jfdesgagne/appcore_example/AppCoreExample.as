package com.jfdesgagne.appcore_example {
	import com.jfdesgagne.appcore.core.AppCore;
	import com.jfdesgagne.appcore.events.ViewEvent;
	import com.jfdesgagne.appcore.net.assetsqueueloader.events.AssetsQueueLoaderEvent;
	import com.jfdesgagne.appcore.net.remoting.services.AbstractAMFService;
	import com.jfdesgagne.appcore.notifications.Notifications;
	import com.jfdesgagne.appcore_example.statics.ViewStatics;
	import com.jfdesgagne.appcore_example.view.MainView;

	/**
	 * @author jfdesgagne
	 */
	public class AppCoreExample extends AppCore {
		protected var __mainView:MainView;

		public function AppCoreExample() {
			//Services settings
			AbstractAMFService.PATH = "http://localhost/amfservices/";
			
			//Assets settings
			__assetQueueLoader.addAsset("appcore_example_assets.swf");
			loadAssets();
		}

		
		override protected function onLoadComplete(e:AssetsQueueLoaderEvent):void {
			super.onLoadComplete(e);
			
			Notifications.notify(new ViewEvent(ViewEvent.CHANGE_VIEW, ViewStatics.MAIN_VIEW));
		}
	}
}
