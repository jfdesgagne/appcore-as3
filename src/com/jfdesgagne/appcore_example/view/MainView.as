package com.jfdesgagne.appcore_example.view {
	import com.jfdesgagne.appcore.data.globals.GlobalsData;
	import com.jfdesgagne.appcore.debug.dtrace;
	import com.jfdesgagne.appcore.events.ViewEvent;
	import com.jfdesgagne.appcore.net.remoting.core.RemotingManager;
	import com.jfdesgagne.appcore.net.remoting.events.RemotingEvent;
	import com.jfdesgagne.appcore.notifications.Notifications;
	import com.jfdesgagne.appcore.view.AbstractBaseView;
	import com.jfdesgagne.appcore_example.data.TestData;
	import com.jfdesgagne.appcore_example.events.NotificationExample;
	import com.jfdesgagne.appcore_example.net.services.Test2Service;
	import com.jfdesgagne.appcore_example.net.services.Test3Service;
	import com.jfdesgagne.appcore_example.net.services.TestService;
	import com.jfdesgagne.appcore_example.statics.ServiceStatics;
	import com.jfdesgagne.appcore_example.statics.ViewStatics;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author jfdesgagne
	 */
	public class MainView extends AbstractBaseView {
		public var openModalButton_mc:MovieClip;		public var switchViewButton_mc:MovieClip;
		protected var __testData:TestData;
		protected var __globalsData:GlobalsData;
		protected var __remotingManager:RemotingManager;
		
		public function MainView() {
			__assetId = "MainView_mc";
			super();
		}

		
		override public function initialize(_initParams:Object):void {
			super.initialize(_initParams);
			
			initData();
			initNotifications();
			initEvents();
			initRemoting();
		}

		private function initRemoting():void {
			__remotingManager = RemotingManager.getInstance();
			__remotingManager.startService(ServiceStatics.SERVICE_TEST, onTestServiceComplete, onTestServiceFail);			__remotingManager.startService(ServiceStatics.SERVICE_TEST2, onTest2ServiceComplete, onTest2ServiceFail);			__remotingManager.startService(ServiceStatics.SERVICE_TEST3, onTest3ServiceComplete, onTest3ServiceFail);
		}
		
		private function onTestServiceComplete(_e:RemotingEvent):void {
			dtrace(TestService(_e.service).receivedTest1);			dtrace(TestService(_e.service).receivedTest2);
			_e.service.destroy();
		}
		
		private function onTestServiceFail(_e:RemotingEvent):void {
			dtrace(_e.service.error);
			_e.service.destroy();
		}
		
		private function onTest2ServiceComplete(_e:RemotingEvent):void {
			dtrace(Test2Service(_e.service).receivedText);
			_e.service.destroy();
		}
		
		private function onTest2ServiceFail(_e:RemotingEvent):void {
			dtrace(_e.service.error);
			_e.service.destroy();
		}
		
		private function onTest3ServiceComplete(_e:RemotingEvent):void {
			var data:Object = Test3Service(_e.service).receivedData;
			for (var element:String in data) {
				dtrace('element: '+element+" is "+data[element]);
			}
			data = null;
			_e.service.destroy();
		}
		
		private function onTest3ServiceFail(_e:RemotingEvent):void {
			dtrace(_e.service.error);
			_e.service.destroy();
		}
		
		private function initEvents():void {
			openModalButton_mc.addEventListener(MouseEvent.CLICK, onOpenModalClickedClicked);
			openModalButton_mc.buttonMode = true;
			
			switchViewButton_mc.addEventListener(MouseEvent.CLICK, onSwitchViewButtonClicked);
			switchViewButton_mc.buttonMode = true;
		}

		private function onOpenModalClickedClicked(_e:MouseEvent):void {
			Notifications.notify(new ViewEvent(ViewEvent.CHANGE_VIEW, ViewStatics.TEST_MODAL));
		}
		
		private function onSwitchViewButtonClicked(_e:MouseEvent):void {
			Notifications.notify(new ViewEvent(ViewEvent.CHANGE_VIEW, ViewStatics.TEST_VIEW));
		}

		private function initNotifications():void {
			Notifications.addNotification(NotificationExample.TEST2, onTest2);
		}

		private function initData():void {
			__testData = new TestData();
			__testData.testString = "before";
			__testData.bind("testString", onUpdateTestString);
			__testData.testString = "before";
			__testData.testString = "end";
			
			__globalsData = GlobalsData.getInstance();
			__globalsData.set("test", "salut");
			dtrace(__globalsData.get("test"));
			
			dtrace("sharedValue1:"+__globalsData.get("saved", GlobalsData.SHARED_OBJECT));			__globalsData.set("saved", "test", GlobalsData.SHARED_OBJECT);
			dtrace("sharedValue2:"+__globalsData.get("saved", GlobalsData.SHARED_OBJECT));
			__globalsData.reset(GlobalsData.SHARED_OBJECT);
		}

		
		override public function close():void {
			super.close();
		}

		
		override public function destroy():void {
			Notifications.removeNotification(NotificationExample.TEST2, onTest2);
			
			if(__testData) {
				__testData.destroy();
				__testData = null;
			}
			
			openModalButton_mc.removeEventListener(MouseEvent.CLICK, onOpenModalClickedClicked);
			openModalButton_mc = null;
			
			switchViewButton_mc.removeEventListener(MouseEvent.CLICK, onSwitchViewButtonClicked);
			switchViewButton_mc = null;
			
			super.destroy();
			
		}

		protected function onUpdateTestString(_oldValue:String, _newValue:String):void {
			trace("onUpdateTestString:", _oldValue, _newValue);
		}
	
		
		private function onTest2(_e:NotificationExample) : void {
			trace('got test2');
		}
	}
}
