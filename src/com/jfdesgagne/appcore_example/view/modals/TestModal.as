package com.jfdesgagne.appcore_example.view.modals {
	import com.jfdesgagne.appcore.assets.AppCoreAsset;
	import com.jfdesgagne.appcore.view.AbstractBaseModalView;
	import com.jfdesgagne.appcore_example.data.TestData;

	import flash.events.MouseEvent;

	/**
	 * @author jfdesgagne
	 */
	public class TestModal extends AbstractBaseModalView {
		public var test:AppCoreAsset;
		protected var __testData:TestData;
		
		public function TestModal() {
			__assetId = "TestModal_mc";
			super();
		}

		
		override public function initialize(_initParams:Object):void {
			super.initialize(_initParams);
			
			initEvents();
		}

		private function initEvents():void {
			buttonMode = true;
			mouseChildren = false;
			addEventListener(MouseEvent.CLICK, onClick);
		}

		private function onClick(_e:MouseEvent):void {
			removeEventListener(MouseEvent.CLICK, onClick);
			close();
		}
	}
}
