package com.jfdesgagne.appcore_example.view {
	import com.jfdesgagne.appcore.components.TextLabel;
	import com.jfdesgagne.appcore.components.TweenButton;
	import com.jfdesgagne.appcore.debug.dtrace;
	import com.jfdesgagne.appcore.events.ViewEvent;
	import com.jfdesgagne.appcore.manager.queuegroup.core.QueueGroupManager;
	import com.jfdesgagne.appcore.notifications.Notifications;
	import com.jfdesgagne.appcore.view.AbstractBaseView;
	import com.jfdesgagne.appcore_example.statics.ViewStatics;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author jfdesgagne
	 */
	public class TestView extends AbstractBaseView {
		public var openModalButton_mc:MovieClip;		public var switchViewButton_mc:TweenButton;
		public var testLabel_mc:TextLabel;
		protected var __queueGroupManager:QueueGroupManager;
		
		public function TestView() {
			__assetId = "TestView_mc";
			super();
		}

		
		override public function assetReady():void {
			testLabel_mc.text = "caca coucou";
			super.assetReady();
		}

		
		override public function initialize(_initParams:Object):void {
			super.initialize(_initParams);
			initEvents();
			initFunctionsQueue();
		}

		private function initFunctionsQueue():void {
			__queueGroupManager = QueueGroupManager.getInstance();
			__queueGroupManager.addFunction(function1, 0, "test", [1, 3, 5]);			__queueGroupManager.addFunction(function2);			__queueGroupManager.addFunction(function3);
			__queueGroupManager.start();
		}
		
		protected function function1(_value:String, _value2:Array):void {
			dtrace('function1:'+_value+", "+_value2);
			
			__queueGroupManager.functionComplete();
		}
		
		protected function function2():void {
			dtrace('function2');
			__queueGroupManager.functionComplete();
		}
		
		protected function function3():void {
			dtrace('function3');
			__queueGroupManager.functionComplete();
		}
		
		private function initEvents():void {
			openModalButton_mc.addEventListener(MouseEvent.CLICK, onOpenModalClickedClicked);
			openModalButton_mc.buttonMode = true;
			
			switchViewButton_mc.addEventListener(MouseEvent.CLICK, onSwitchViewButtonClicked);
			switchViewButton_mc.buttonMode = true;
		}

		private function onOpenModalClickedClicked(_e:MouseEvent):void {
			Notifications.notify(new ViewEvent(ViewEvent.CHANGE_VIEW, ViewStatics.TEST_MODAL));
		}
		
		private function onSwitchViewButtonClicked(_e:MouseEvent):void {
			Notifications.notify(new ViewEvent(ViewEvent.CHANGE_VIEW, ViewStatics.MAIN_VIEW));
		}

		
	}
}
