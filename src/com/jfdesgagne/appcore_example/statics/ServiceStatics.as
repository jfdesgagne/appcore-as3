package com.jfdesgagne.appcore_example.statics {
	import com.jfdesgagne.appcore_example.net.services.Test2Service;
	import com.jfdesgagne.appcore_example.net.services.Test3Service;
	import com.jfdesgagne.appcore_example.net.services.TestService;

	/**
	 * @author jfdesgagne
	 */
	public class ServiceStatics {
		public static const SERVICE_TEST:Class = TestService;		public static const SERVICE_TEST2:Class = Test2Service;		public static const SERVICE_TEST3:Class = Test3Service;
		
	}
}
