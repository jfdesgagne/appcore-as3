package com.jfdesgagne.appcore_example.statics {
	import com.jfdesgagne.appcore_example.view.MainView;
	import com.jfdesgagne.appcore_example.view.TestView;
	import com.jfdesgagne.appcore_example.view.modals.TestModal;

	/**
	 * @author jfdesgagne
	 */
	public class ViewStatics {
		//Views
		public static const MAIN_VIEW:Class = MainView;		public static const TEST_VIEW:Class = TestView;
		
		//Modals		public static const TEST_MODAL:Class = TestModal;
	}
}
