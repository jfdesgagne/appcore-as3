package com.jfdesgagne.appcore_example.events {
	import flash.events.Event;

	/**
	 * @author jfdesgagne
	 */
	public class NotificationExample extends Event {
		public static const TEST1:String = "test1";		public static const TEST2:String = "test2";
		
		public function NotificationExample(type : String, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
		}
	}
}
